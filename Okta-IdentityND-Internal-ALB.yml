Description: >
    For Okta-IdentityND-Internal-ALB (CRC and SCIM and ...).
    It assumes that various entities already exist:  Security Groups, Instances, Target Groups, SSL Certificates and S3 Buckets
    for LB logs.

Parameters:

    LBName:
        Description: An environment name that will be prefixed to resource names
        Type: String

    VPC:
        Type: AWS::EC2::VPC::Id
        Description: Choose which VPC the Application Load Balancer should be deployed to

    Subnets:
        Description: Choose which subnets the Application Load Balancer should be deployed to
        Type: List<AWS::EC2::Subnet::Id>

    SecurityGroup:
        Description: Select the Security Group to apply to the Application Load Balancer
        Type: List<AWS::EC2::SecurityGroup::Id>


Resources:

    #In the main LoadBalancer (LB) section, the LB type is chosen (internal/expernal), various attributes are configured
    #(deletion protection, logging details, subnets, SecurityGroups, Tags...)

    LoadBalancer:
        Type: AWS::ElasticLoadBalancingV2::LoadBalancer
        Properties:
            Name: !Ref LBName
            Scheme: internal
            LoadBalancerAttributes:
                - Key:  deletion_protection.enabled
                  Value: 'true'
                - Key:  access_logs.s3.enabled
                  Value: 'true'
                - Key:  access_logs.s3.bucket
                  Value: 'elb-logs-identitynd'
                - Key:  access_logs.s3.prefix
                  Value: 'Okta-IdentityND-Internal-ALB'
                - Key:  routing.http2.enabled
                  Value: 'true'
                - Key:  idle_timeout.timeout_seconds
                  Value: '1800'
            Subnets: !Ref Subnets
            SecurityGroups: !Ref SecurityGroup
            Tags:
                - Key: Name
                  Value: !Ref LBName
                - Key: owner
                  Value: 'neteng'



    #The LoadBalancerListener resource is where the incoming TCP port is designated along with any SSL Certificates that are needed and the DefaultTargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn, Fn::ImportValue part of the Listener

    LoadBalancerListener1:
        Type: AWS::ElasticLoadBalancingV2::Listener
        Properties:
            LoadBalancerArn: !Ref LoadBalancer
            Port: 443
            Protocol: HTTPS
            Certificates:
               - CertificateArn: arn:aws:acm:us-east-1:949972330534:certificate/3af0734a-e1ed-4204-9330-451ae86c5fd9
            DefaultActions:
                - Type: forward
                  TargetGroupArn:
                    Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNOktaBlankTGcf

    #LoadBalancerListener2:
        #Type: AWS::ElasticLoadBalancingV2::Listener
        #Properties:
            #LoadBalancerArn: !Ref LoadBalancer
            #Port: 80
            #Protocol: HTTP
            #Certificates:
               #- CertificateArn:
            #DefaultActions:
                #- Type: redirect
                  #RedirectConfig:
                    #Protocol: HTTPS
                    #Port: "443"
                    #Host: "#{host}"
                    #Path: "/#{path}"
                    #Query: "#{query}"
                    #StatusCode: "HTTP_302"
                  #TargetGroupArn:
                    #Fn::ImportValue: BNR-dev-TargetGroups-TargetGroupARNBNRSSBdev8080ALBcf


    #The ListenerRule resource is used when traffic needs to be routed to a different TargetGroup other than the DefaultTargetGroup
    #specified in the LoadBalancerListener resource.  Under Conditions, the ListenerRule either matches a url (host-header) or uri (path) string to route
    #traffic to the specified ListenerRule TargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn,
    #Fn::ImportValue part of the Listener

    ListenerRule1a:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNSCIMCRCKerberos81cf
           Conditions:
           - Field: host-header
             Values:
             - "scim-crckerberos.oit.nd.edu"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 1

    ListenerRule1b:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNWSCRCKAdmin9600cf
           Conditions:
           - Field: host-header
             Values:
             - "ws-crckadmin.oit.nd.edu"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 2

    ListenerRule1c:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNSCIMbnrjson81cf
           Conditions:
           - Field: host-header
             Values:
             - "scim-bannerjson.oit.nd.edu"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 3

    ListenerRule1d:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNSCIMbnrpush82cf
           Conditions:
           - Field: host-header
             Values:
             - "scim-bannerpush.oit.nd.edu"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 4

    ListenerRule1e:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNoktandpvidlambdacf
           Conditions:
           - Field: host-header
             Values:
             - "acgen.oit.nd.edu"
           - Field: path-pattern
             Values:
             - "/api"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 5

    ListenerRule1f:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNlegacyutilitylambdacf
           Conditions:
           - Field: host-header
             Values:
             - "acgen.oit.nd.edu"
           - Field: path-pattern
             Values:
             - "/legacy"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 6

    ListenerRule1g:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-IdentityND-Internal-TargetGroups-TargetGroupARNoktaresendlambdacf
           Conditions:
           - Field: host-header
             Values:
             - "acgen.oit.nd.edu"
           - Field: path-pattern
             Values:
             - "/resendmail"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 7

Outputs:

    LoadBalancer:
        Description: A reference to the Application Load Balancer
        Value: !Ref LoadBalancer

    LoadBalancerUrl:
        Description: The URL of the ALB
        Value: !GetAtt LoadBalancer.DNSName

    Listener1:
        Description: A reference to a port 80 listener
        Value: !Ref LoadBalancerListener1

    #Listener2:
        #Description: A reference to a port 80 listener
        #Value: !Ref LoadBalancerListener2
