Description: >
    For LRW-Test
    This template deploys an Application Load Balancer for LRW Test services.
    It assumes that various entities already exist:  Security Groups, Instances, Target Groups, SSL Certificates and S3 Buckets
    for LB logs.

Parameters:

    LBName:
        Description: An environment name that will be prefixed to resource names
        Type: String

    VPC:
        Type: AWS::EC2::VPC::Id
        Description: Choose which VPC the Application Load Balancer should be deployed to

    Subnets:
        Description: Choose which subnets the Application Load Balancer should be deployed to
        Type: List<AWS::EC2::Subnet::Id>

    SecurityGroup:
        Description: Select the Security Group to apply to the Application Load Balancer
        Type: List<AWS::EC2::SecurityGroup::Id>


Resources:

    #In the main LoadBalancer (LB) section, the LB type is chosen (internal/expernal), various attributes are configured
    #(deletion protection, logging details, subnets, SecurityGroups, Tags...)

    LoadBalancer:
        Type: AWS::ElasticLoadBalancingV2::LoadBalancer
        Properties:
            Name: !Ref LBName
            Scheme: internet-facing
            LoadBalancerAttributes:
                - Key:  deletion_protection.enabled
                  Value: 'true'
                - Key:  access_logs.s3.enabled
                  Value: 'true'
                - Key:  access_logs.s3.bucket
                  Value: 'elb.logs.dcnd'
                - Key:  access_logs.s3.prefix
                  Value: 'LRW-Test'
            Subnets: !Ref Subnets
            SecurityGroups: !Ref SecurityGroup
            Tags:
                - Key: Name
                  Value: !Ref LBName
                - Key: owner
                  Value: 'neteng'



    #The LoadBalancerListener resource is where the incoming TCP port is designated along with any SSL Certificates that are needed and the DefaultTargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn, Fn::ImportValue part of the Listener

    LoadBalancerListener1:
        Type: AWS::ElasticLoadBalancingV2::Listener
        Properties:
            LoadBalancerArn: !Ref LoadBalancer
            Port: 443
            Protocol: HTTPS
            Certificates:
               - CertificateArn: arn:aws:acm:us-east-1:426280549303:certificate/f13f8110-23e2-4ced-a9ac-8ae370b3d7ae
            DefaultActions:
                - Type: forward
                  TargetGroupArn:
                    Fn::ImportValue: LRW-Test-TargetGroups-TargetGroupARNLRWtest443ALBcf

    #LoadBalancerListener2:
        #Type: AWS::ElasticLoadBalancingV2::Listener
        #Properties:
            #LoadBalancerArn: !Ref LoadBalancer
            #Port: 8443
            #Protocol: HTTPS
            #Certificates:
               #- CertificateArn: arn:aws:acm:us-east-1:120182368751:certificate/966105ab-7fe2-4944-a7f2-ac6b28e9622b
            #DefaultActions:
                #- Type: forward
                  #TargetGroupArn:
                    #Fn::ImportValue: AIM-Prod-TargetGroups-TargetGroupARNaim18443ALBcf


    #The ListenerRule resource is used when traffic needs to be routed to a different TargetGroup other than the DefaultTargetGroup
    #specified in the LoadBalancerListener resource.  Under Conditions, the ListenerRule either matches a url (host-header) or uri (path) string to route
    #traffic to the specified ListenerRule TargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn,
    #Fn::ImportValue part of the Listener

    #ListenerRule1a:
        #Type: AWS::ElasticLoadBalancingV2::ListenerRule
        #Properties:
           #Actions:
           #- Type: forward
             #TargetGroupArn:
               #Fn::ImportValue: InvestCRM-test-TargetGroups-TargetGroupARNInvestCRMrptstest443ALBcf
           #Conditions:
           #- Field: host-header
             #Values:
             #- "investcrm-rpts-test.oit.nd.edu"
           #ListenerArn:
             #Ref: LoadBalancerListener1
           #Priority: 1



    #ListenerRule2a:
            #Type: AWS::ElasticLoadBalancingV2::ListenerRule
            #Properties:
               #Actions:
               #- Type: forward
                 #TargetGroupArn:
                   #Fn::ImportValue: AIM-NP-TargetGroups-TargetGroupARNaim1dev8443ALBcf
               #Conditions:
               #- Field: host-header
                 #Values:
                 #- "aim1-dev.oit.nd.edu"
               #ListenerArn:
                 #Ref: LoadBalancerListener2
               #Priority: 1


Outputs:

    LoadBalancer:
        Description: A reference to the Application Load Balancer
        Value: !Ref LoadBalancer

    LoadBalancerUrl:
        Description: The URL of the ALB
        Value: !GetAtt LoadBalancer.DNSName

    Listener1:
        Description: A reference to a port 443 listener
        Value: !Ref LoadBalancerListener1

    #Listener2:
        #Description: A reference to a port 8443 listener
        #Value: !Ref LoadBalancerListener2
