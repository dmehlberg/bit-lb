Description: >
    For Okta-Microservices (netId, facade, ndpvid, uuid and legacy_accgen_okta_utility).
    It assumes that various entities already exist:  Security Groups, Lambdas, Target Groups, SSL Certificates and S3 Buckets
    for LB logs.

Parameters:

    LBName:
        Description: An environment name that will be prefixed to resource names
        Type: String

    VPC:
        Type: AWS::EC2::VPC::Id
        Description: Choose which VPC the Application Load Balancer should be deployed to

    Subnets:
        Description: Choose which subnets the Application Load Balancer should be deployed to
        Type: List<AWS::EC2::Subnet::Id>

    SecurityGroup:
        Description: Select the Security Group to apply to the Application Load Balancer
        Type: List<AWS::EC2::SecurityGroup::Id>


Resources:

    #In the main LoadBalancer (LB) section, the LB type is chosen (internal/expernal), various attributes are configured
    #(deletion protection, logging details, subnets, SecurityGroups, Tags...)

    LoadBalancer:
        Type: AWS::ElasticLoadBalancingV2::LoadBalancer
        Properties:
            Name: !Ref LBName
            Scheme: internet-facing
            LoadBalancerAttributes:
                - Key:  deletion_protection.enabled
                  Value: 'true'
                - Key:  access_logs.s3.enabled
                  Value: 'true'
                - Key:  access_logs.s3.bucket
                  Value: 'elb-logs-identitynd'
                - Key:  access_logs.s3.prefix
                  Value: 'Okta-Microservices-ALB'
                - Key:  routing.http2.enabled
                  Value: 'true'
                - Key:  idle_timeout.timeout_seconds
                  Value: '1800'
            Subnets: !Ref Subnets
            SecurityGroups: !Ref SecurityGroup
            Tags:
                - Key: Name
                  Value: !Ref LBName
                - Key: owner
                  Value: 'neteng'



    #The LoadBalancerListener resource is where the incoming TCP port is designated along with any SSL Certificates that are needed and the DefaultTargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn, Fn::ImportValue part of the Listener

    LoadBalancerListener1:
        Type: AWS::ElasticLoadBalancingV2::Listener
        Properties:
            LoadBalancerArn: !Ref LoadBalancer
            Port: 80
            Protocol: HTTP
            #Certificates:
               #- CertificateArn: arn:aws:acm:us-east-1:426280549303:certificate/f13f8110-23e2-4ced-a9ac-8ae370b3d7ae
            DefaultActions:
                - Type: fixed-response
                  FixedResponseConfig:
                    StatusCode: "503"
                    ContentType: "text/plain"
                    MessageBody: "Url does not exist"


    #The ListenerRule resource is used when traffic needs to be routed to a different TargetGroup other than the DefaultTargetGroup
    #specified in the LoadBalancerListener resource.  Under Conditions, the ListenerRule either matches a url (host-header) or uri (path) string to route
    #traffic to the specified ListenerRule TargetGroup.
    #The TargetGroups are imported via another CF template (AIM-NP-TargetGroups.yml) via the TargetGroupArn,
    #Fn::ImportValue part of the Listener

    ListenerRule1a:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNoktanetidlambdacf
           Conditions:
           #- Field: host-header
             #Values:
             #- "jobcode-dev18.nd.edu"
           - Field: path-pattern
             Values:
             - "/api/netid"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 1

    ListenerRule1b:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNoktafacadelambdacf
           Conditions:
           #- Field: host-header
             #Values:
             #- "jobcode-dev18.nd.edu"
           - Field: path-pattern
             Values:
             - "/api/facade"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 2

    ListenerRule1c:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNoktandpvidlambdacf
           Conditions:
           #- Field: host-header
             #Values:
             #- "jobcode-dev18.nd.edu"
           - Field: path-pattern
             Values:
             - "/api/ndPVid"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 3

    ListenerRule1d:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNoktauuidlambdacf
           Conditions:
           #- Field: host-header
             #Values:
             #- "jobcode-dev18.nd.edu"
           - Field: path-pattern
             Values:
             - "/api/uuid"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 4

    ListenerRule1e:
        Type: AWS::ElasticLoadBalancingV2::ListenerRule
        Properties:
           Actions:
           - Type: forward
             TargetGroupArn:
               Fn::ImportValue: Okta-Microservices-TargetGroups-TargetGroupARNlegacyutilitylambdacf
           Conditions:
           #- Field: host-header
             #Values:
             #- "jobcode-dev18.nd.edu"
           - Field: path-pattern
             Values:
             - "/api/legacy"
           ListenerArn:
             Ref: LoadBalancerListener1
           Priority: 5


Outputs:

    LoadBalancer:
        Description: A reference to the Application Load Balancer
        Value: !Ref LoadBalancer

    LoadBalancerUrl:
        Description: The URL of the ALB
        Value: !GetAtt LoadBalancer.DNSName

    Listener1:
        Description: A reference to a port 80 listener
        Value: !Ref LoadBalancerListener1

    #Listener2:
        #Description: A reference to a port 80 listener
        #Value: !Ref LoadBalancerListener2
